package apiwat.kitchanukit.kku.ac.th.myapplication3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    RadioGroup group;
    RadioButton add,minus,multiply,divide;
    EditText ed1,ed2;
    TextView text,ts;
    Button cal;
    double num1,num2,sum;
    String msg,status;
    int val1,val2;
    Switch work;
    Toast toast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ed1 = (EditText)findViewById(R.id.ed1);
        ed2 = (EditText)findViewById(R.id.ed2);
        text = (TextView)findViewById(R.id.text);


        //radio
        group = (RadioGroup)findViewById(R.id.group);
        add = (RadioButton)findViewById(R.id.add);// +
        minus = (RadioButton)findViewById(R.id.minus);//-
        multiply = (RadioButton)findViewById(R.id.multiply);//*
        divide = (RadioButton)findViewById(R.id.divide);// /


        cal = (Button)findViewById(R.id.cal);//Button Calculator

        work = (Switch) findViewById(R.id.work);//Switch
        ts = (TextView)findViewById(R.id.ts);//text Switch


        //check Radio
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                acceptNumber();

                calculate(i);
            }

        });

        //Check Button Calculator
        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                acceptNumber();
                calculate(group.getCheckedRadioButtonId());
            }
        });

        //Check Switch
        work.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status = "ON";
                    ts.setText(status);
                   acceptNumber();
                    calculate(group.getCheckedRadioButtonId());
                }else{
                    status = "OFF";
                    ts.setText(status);
                }

            }
        });
        }


    //Calculate
    public void calculate(int id) {

        long start = System.currentTimeMillis();

        switch (id) {

            case R.id.add:
                sum = num1 + num2;
                break;
            case R.id.minus:
                sum = num1 - num2;
                break;
            case R.id.multiply:
                sum = num1 * num2;
                break;
            case R.id.divide:

                if(num2 == 0){
                    sum = 0;
                    showToast("Please divide by a non-zero number");
                    break;
                }
                sum = num1 / num2;
                break;

        }

        text.setText("= " + String.valueOf(sum));

        long end = System.currentTimeMillis();

        android.util.Log.d("TimeClass" ,"TimeValue"+ (start - end));
    }


    //Show Toast
    public void showToast(String msg){
        toast = Toast.makeText(getApplicationContext(),msg, Toast.LENGTH_LONG);
        toast.show();

    }

    //accept input
    private void acceptNumber(){

        try{
            num1 = Double.parseDouble(ed1.getText().toString());
            num2 = Double.parseDouble((ed2.getText().toString()));
        }catch(NumberFormatException e){
            showToast("Please enter only a number");
        }
    }

}
